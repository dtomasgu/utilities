#!/bin/bash


# Run on root devstack folder
REPOS=(`ls -d */`)

for d in ${REPOS[@]};
do
    cd "$d"
    git status
    if [ $? -eq 0 ]; then
        git pull
    fi
    cd ..
done
