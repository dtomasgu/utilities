#!/bin/bash

echo "##################################################################"
echo "Welcome to the devstack env management utility"
echo "This script will assume that you have configured:"
echo "1. Kerberos authentication to the lxplus-cloud on this computer"
echo "2. Created an id_rsa key and an openstack keypair from the lxplus-cloud"
echo "If this is not your case take the time to do it now."
echo "##################################################################"
read -p "Press any key to continue..." -n 1 -r

################################################# ENTERING CERN USER
read -p "Please type your Kerberos user: " KERBEROS_USERNAME
echo "A devstack env includes the env.sh, devstack repo mounted on this machine"
echo "with fuse and a kubernetes access store."

################################################# ENTERING DEVSTACK DEV
USER_ROOT_PATH="/afs/cern.ch/user/${KERBEROS_USERNAME:0:1}/${KERBEROS_USERNAME}"
DEVSTACK_ENV_PATH="${USER_ROOT_PATH}/ws"
echo "Your workspace is stored at ${DEVSTACK_ENV_PATH}"


echo "##################################################################"
echo "This are the projects you have access to:"
ssh ${KERBEROS_USERNAME}@lxplus-cloud.cern.ch bash -c "'
source \${HOME}/.bashrc
openstack project list
'"
echo "##################################################################"

IFS= read -r -p  "In which project should i create your VMs? (Name Only): " OS_PROJECT_NAME

read -p "What is the openstack keypair name I should use?: " OS_KEYPAIR_NAME

sed -i "s|\${KERBEROS_USERNAME}|${KERBEROS_USERNAME}|g" .local_bash_utils
sed -i "s|\${KERBEROS_USERNAME:0:1}|${KERBEROS_USERNAME:0:1}|g" .local_bash_utils
sed -i "s|\${OS_PROJECT_NAME}|${OS_PROJECT_NAME}|g" .local_bash_utils
sed -i "s|\${OS_KEYPAIR_NAME}|${OS_KEYPAIR_NAME}|g" .local_bash_utils
sed -i "s|\${DEVSTACK_ENV_PATH}|${DEVSTACK_ENV_PATH}|g" .local_bash_utils

################################################# ENTERING DEVSTACK CLOUD.INIT
## TODO: CONFIG AND ADD CLOUD INIT FILE TO LXPLUS-CLOUD
read -p "Would you like to receive an e-mail each time your VM env completes setup? [y/n]: " -n 1 -r

if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo ""
    read -p "What is the email address I should send this message to?: " EMAIL_ADDRESS
    sed -i "s/\${EMAIL_ADDRESS}/${EMAIL_ADDRESS}/g" devstack_cloud.init
fi

echo ""
IFS= read -r -p  "To enable easy access to your created machines, add your public key:
" ID_RSA_PUB

ID_RSA_PUB=$(echo $ID_RSA_PUB | sed -e  "s|/|\\\\/|g")
sed -i "s/\${ID_RSA_PUB}/${ID_RSA_PUB}/g" devstack_cloud.init

########### All PARAMETERS OK::::: MOVING AND SETTING UP FILES
################################################# MOVE CLOUD INIT TO REMOTE
echo "##################################################################"
echo "Copying devstack_cloud.init into your afs workspace..."
mkdir -p ${DEVSTACK_ENV_PATH}/config
scp -r devstack_cloud.init ${KERBEROS_USERNAME}@lxplus.cern.ch:${DEVSTACK_ENV_PATH}/config/devstack_cloud.init

echo "##################################################################"
echo "Copying local bash utilities..."
################################################# ADD TO LOCAL BASHRC
cp .local_bash_utils ${HOME}/.local_bash_utils
cat >> ${HOME}/.bashrc << END


#### LOAD DEVSTACK CONFIGURED PARAMETERS
source \${HOME}/.local_bash_utils

END

################################################# ALL DONE
echo "##################################################################"
echo "#### ALL DONE"
source ~/.bashrc
