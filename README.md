# Launching a machine from openstack with functional devstack and other utilities

## Create your cluster with a devstack cloud-init file
  * Copy the devstack_cloud.init into your local openstack accessible computer.
  * Update the devstack_cloud.init file (Follow the #TODO Markers to help you on your quest):
    *  Change your ssh-rsa authorized key for your own id_rsa.pub key.
        To get it, on your local computer run:
            `cat $HOME/.ssh/id_rsa.pub`
        if it does not exist create one:
            `ssh-keygen -t rsa -N '' -f $HOME/.ssh/id_rsa`
      * Personalize your bash.
      * Edit your local.conf file for devstack component selection.
      * Save your configurations.
  * Now, create your VM:
    `openstack server create --flavor m2.xlarge --image 57f01b3c-2393-40b5-8b5d-1e408a9022f1 --key-name <YOUR_KEY> <YOUR_INSTANCE_NAME> --property cern-services='true' --user-data devstack_cloud.init`
