#!/bin/bash

cat >> $HOME/.bashrc <<EOF



#Generic
source /opt/stack/devstack/openrc admin admin
export EDITOR=vim
source <(kubectl completion bash)
source <(openstack complete)

# Kubernetes
sk8s() { rm config; `openstack coe cluster config \$1`; source <(kubectl completion bash); }
alias dk8s='rm ca.pem cert.pem config key.pem';

alias ka="kubectl --all-namespaces=true"
alias kn="kubectl -n "
alias ks="kubectl -n kube-system"
alias km="kubectl -n magnum-tiller"
alias kga="clear && kubectl get all --all-namespaces "

# Devstack
alias restart_magnum='sudo systemctl restart devstack@magnum-api.service && sudo systemctl restart devstack@magnum-cond.service'



EOF
