#!/bin/bash -xe

cat > /opt/stack/devstack/local.conf <<END
[[local|localrc]]

IP_VERSION=4
SERVICE_IP_VERSION=4
PUBLIC_INTERFACE=em1

DATABASE_PASSWORD=password
RABBIT_PASSWORD=password
SERVICE_TOKEN=password
SERVICE_PASSWORD=password
ADMIN_PASSWORD=password

enable_service tls-proxy

enable_plugin barbican https://git.openstack.org/openstack/barbican

enable_plugin neutron-lbaas https://git.openstack.org/openstack/neutron-lbaas
enable_plugin octavia https://git.openstack.org/openstack/octavia
disable_service q-lbaas
enable_service q-lbaasv2
enable_service octavia
enable_service o-cw
enable_service o-hk
enable_service o-hm
enable_service o-api

disable_service horizon

enable_plugin heat https://git.openstack.org/openstack/heat
enable_plugin magnum https://git.openstack.org/openstack/magnum

ENABLED_SERVICES+=,octavia,o-cw,o-hk,o-hm,o-api
ENABLED_SERVICES+=,q-svc,q-agt,q-dhcp,q-l3,q-meta
ENABLED_SERVICES+=,q-lbaasv2


VOLUME_BACKING_FILE_SIZE=20G
END

cd /opt/stack/devstack
time ./stack.sh

cat >> /opt/stack/.bashrc <<EOF
source /opt/stack/devstack/openrc admin admin
export EDITOR=vim
source <(kubectl completion bash)
source <(openstack complete)
EOF

ssh-keygen -t rsa -N '' -f /opt/stack/.ssh/id_rsa
openstack keypair create --public-key ~/.ssh/id_rsa.pub default
