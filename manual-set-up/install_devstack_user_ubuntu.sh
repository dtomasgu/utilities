#!/bin/bash -xe

#openstack server create --flavor m2.2xlarge --image 57f01b3c-2393-40b5-8b5d-1e408a9022f1 --key-name dtomasgu-key dtomasgu-dev --property cern-services='false'

sudo apt-get update && \
sudo apt-get install git -y


git clone https://git.openstack.org/openstack-dev/devstack
sudo devstack/tools/create-stack-user.sh

sudo cp -r devstack /opt/stack
sudo chown -R stack:stack /opt/stack/devstack

# sudo su stack
sudo su -s /bin/sh -c "mkdir -p /opt/stack/.ssh" stack
sudo su -s /bin/sh -c "chmod 700 /opt/stack/.ssh" stack
sudo su -s /bin/sh -c "cat > /opt/stack/.ssh/authorized_keys << END
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC2oOPozRjZGPAo94wPhhqzPGK5wsxtUHeS0whosEiXQkDUsRLOCVCU683UW5/B0e/PWdWaEIRt31MWA7hp8Ypn+ba0tf7O1kZUNmxmG44VegOC1gF5WoaSoW80kZ5BnGvHt4B8ybL8XZA9rUHr08xRj/T7PsAouZa/oPE/Ja/PUQT+CSWDjrGjr2jx8uvA+pyLCB66e2+Esnjp/wQA6xq+dWWw0Nh7X/bh3a0b+zRawV3Wr6vqDXw2fmoqLou7zjgmrJzAq2KRD6mW5EFjkBcVBNnUf5U5zekXcHOqWnBiOe7pp/bSR+RGPsoMSE3te+ENeG0mQgqsGkk5iRkk3eZz
END" stack

#exit 0
# helper with ssh
cat > ~/ssh_no <<EOF
ssh -4 -o  StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \$@
EOF
chmod +x ~/ssh_no
cat > ~/scp_no <<EOF
scp -o  StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \$@
EOF
chmod +x ~/scp_no
sudo mv ~/ssh_no /usr/local/bin/
sudo mv ~/scp_no /usr/local/bin/

# kubectl
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl

sudo apt update &&
sudo apt install -y python-dev libssl-dev libxml2-dev \
                    libmysqlclient-dev libxslt-dev libpq-dev git \
                    libffi-dev gettext build-essential python3.5-dev

curl -s https://bootstrap.pypa.io/get-pip.py | sudo python
sudo pip install virtualenv flake8 tox testrepository git-review
sudo pip install -U virtualenv
